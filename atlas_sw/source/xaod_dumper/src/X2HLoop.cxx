//xaod_dumper
#include "xaod_dumper/X2HLoop.h"
#include "xaod_dumper/utility/misc_helpers.h"

//std/stl
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <math.h>
using namespace std;

//xAOD
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "PathResolver/PathResolver.h"
#include "xAODMetaData/FileMetaData.h"
#include "xAODBase/IParticleHelpers.h"

const static SG::AuxElement::ConstAccessor<char> acc_bad("bad");
const static SG::AuxElement::ConstAccessor<char> acc_cosmic("cosmic");
const static SG::AuxElement::ConstAccessor<char> acc_baseline("baseline");
const static SG::AuxElement::ConstAccessor<char> acc_signal("signal");
const static SG::AuxElement::ConstAccessor<char> acc_passOR("passOR");
const static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");

using GhostList_t = std::vector< ElementLink<xAOD::IParticleContainer> >;
const static SG::AuxElement::ConstAccessor<GhostList_t> acc_ghostTrack("GhostTrack");
typedef ElementLink<xAOD::IParticleContainer> iplink_t;
const static SG::AuxElement::ConstAccessor<iplink_t> acc_originalObject("originalObjectLink");
const static SG::AuxElement::ConstAccessor< std::vector<iplink_t> > acc_ghostMuons("GhostMuon");
const static SG::AuxElement::ConstAccessor< std::vector<iplink_t> > acc_ghostElecs("GhostElec");
const static SG::AuxElement::ConstAccessor<float> acc_jetWidth("Width");
const static SG::AuxElement::ConstAccessor< std::vector<int> > acc_trkN("NumTrkPt500");
const static SG::AuxElement::ConstAccessor< std::vector<float> > acc_trksumpt("SumPtTrkPt500");
const static SG::AuxElement::ConstAccessor< std::vector<float> > acc_sampleE("EnergyPerSampling");
const static SG::AuxElement::Decorator<float> dec_truthHadronPt("truthHadronPt");
const static SG::AuxElement::Decorator<float> acc_truthHadronPt("truthHadronPt");

const static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");
const static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");

const static SG::AuxElement::ConstAccessor<unsigned int> acc_classifierParticleOrigin("classifierParticleOrigin");
const static SG::AuxElement::ConstAccessor<unsigned int> acc_classifierParticleType("classifierParticleType");
const static SG::AuxElement::ConstAccessor<unsigned int> acc_classiferParticleOutCome("classifierParticleOutCome");
const static SG::AuxElement::ConstAccessor<unsigned int> acc_particleMotherPdgId("particleMotherPdgId");
const static SG::AuxElement::ConstAccessor<int> acc_PartonTruthLabelID("PartonTruthLabelID");

//bjet
const static SG::AuxElement::ConstAccessor<int> acc_jetTruthLabel("HadronConeExclTruthLabelID");

//muons
const static SG::AuxElement::ConstAccessor<float> acc_msPt("MuonSpectrometerPt");
const static SG::AuxElement::ConstAccessor<float> acc_idPt("InnerDetectorPt");

//isolation variable accessors
static SG::AuxElement::ConstAccessor<float> acc_ptvarcone20("ptvarcone20");
static SG::AuxElement::ConstAccessor<float> acc_ptcone20("ptcone20");
static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30("ptvarcone30");
static SG::AuxElement::ConstAccessor<float> acc_ptcone30("ptcone30");
static SG::AuxElement::ConstAccessor<float> acc_ptvarcone40("ptvarcone40");
static SG::AuxElement::ConstAccessor<float> acc_ptcone40("ptcone40");
static SG::AuxElement::ConstAccessor<float> acc_etcone20("etcone20");
static SG::AuxElement::ConstAccessor<float> acc_topoetcone20("topoetcone20");
static SG::AuxElement::ConstAccessor<float> acc_topoetcone30("topoetcone30");
static SG::AuxElement::ConstAccessor<float> acc_topoetcone40("topoetcone40");
static SG::AuxElement::ConstAccessor<float> acc_ptvarcone20_TightTTVA_pt1000("ptvarcone20_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30_TightTTVA_pt1000("ptvarcone30_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30_TightTTVA_pt500("ptvarcone30_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor<float> acc_ptcone20_TightTTVA_pt1000("ptcone20_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor<float> acc_ptcone20_TightTTVA_pt500("ptcone20_TightTTVA_pt500");

namespace bmo
{
//____________________________________________________________________________
X2HLoop::X2HLoop() :
    m_verbose(false),
    m_mujetcorr(false),
    m_dsid(0),
    m_outdir("./"),
    m_suffix(""),
    _susyObj(nullptr),
    _electrons(0),
    _electrons_aux(0),
    _muons(0),
    _muons_aux(0),
    _jets(0),
    _jets_aux(0),
    n_events_processed(0),
    n_jets_stored(0)
{
    cout << BMOFUNC << endl;
    m_event = new xAOD::TEvent(xAOD::TEvent::kClassAccess);
    m_store = new xAOD::TStore();

    //h = new TH1F("h", "", 100, 0, -1);
}
//____________________________________________________________________________
bool X2HLoop::set_mc_campaign(string c)
{
    vector<string> allowed_mc = { "mc16a", "mc16d", "mc16e" };
    if(std::find(allowed_mc.begin(), allowed_mc.end(), c) == allowed_mc.end())
    {
        cout << BMOFUNC << "    ERROR Provided MC campaign (=" << c << ") is not allowed" << endl;
        return false;
    }
    m_mc_campaign = c;
    return true;
}
//____________________________________________________________________________
string X2HLoop::timer_summary()
{
    double realTime = timer()->RealTime();
    double cpuTime = timer()->CpuTime();
    int hours = int(realTime / 3600);
    realTime -= hours * 3600;
    int min = int(realTime / 60);
    realTime -= min * 60;
    int sec = int(realTime);
    float jet_rate = n_jets_stored / timer()->RealTime() / 1000;
    float evt_rate = n_events_processed / timer()->RealTime() / 1000;

    TString line1; line1.Form("Real %d:%02d:%02d, CPU %.3f", hours, min, sec, cpuTime);
    TString line2; line2.Form("%2.3f", evt_rate);
    TString line3; line3.Form("%2.3f", jet_rate);
    ostringstream oss;
    oss << BMOFUNC << " ---------------------------------------------------------------" << endl;
    oss << BMOFUNC << "  Number of events processed   : " << n_events_processed << endl;
    oss << BMOFUNC << "  Number of jets stored        : " << n_jets_stored << endl;
    oss << BMOFUNC << "  Analysis time                : " << line1 << endl;
    oss << BMOFUNC << "  Event rate [kHz]             : " << line2 << endl;
    oss << BMOFUNC << "  Jet rate [kHz]               : " << line3 << endl;
    oss << BMOFUNC << " ---------------------------------------------------------------" << endl;
    return oss.str();
}
//____________________________________________________________________________
vector<string> X2HLoop::ilumicalc_files()
{
    vector<string> lumi_calcs;
    
    bool lumi_calc_ok = true;
    
    // 2015
    string lumi_calc_2015_loc = "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root";
    string lumi_calc_2015 = PathResolverFindCalibFile(lumi_calc_2015_loc); // FindDataFile
    if(lumi_calc_2015=="") {
        cout << BMOFUNC << "    ERROR PathResolver unable to find "
            << "2015 ilumicalc file (=" << lumi_calc_2015_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2015);
    
    // 2016
    string lumi_calc_2016_loc = "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root";
    string lumi_calc_2016 = PathResolverFindCalibFile(lumi_calc_2016_loc);
    if(lumi_calc_2016=="") {
        cout << BMOFUNC << "    ERROR PathResolver unable to find "
            << "2016 ilumicalc file (=" << lumi_calc_2016_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2016);
    
    // 2017
    // dantrim September 25 2018 : actualMu files go under the "PRW configs" not the "LumiCalcFiles" but we still need to add the averageMu files
    // see this talk for clarification: https://indico.cern.ch/event/712774/contributions/2928042/attachments/1614637/2565496/prw_mc16d.pdf
    string lumi_calc_2017_loc = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"; // lumi-calc
    string lumi_calc_2017 = PathResolverFindCalibFile(lumi_calc_2017_loc); // FindDataFile
    if(lumi_calc_2017=="") {
        cout << BMOFUNC << "    ERROR PathResolver unable to find "
            << "2017 ilumicalc file (=" << lumi_calc_2017_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2017);
    
    // dantrim 2018/10/28 - for n0305 i do not expect to launch mc16e samples, so do not load the 2018 mu profiles (in this way we can
    // get the correct combined mc16a+mc16d PRW for compairing to 2015+2016+2018 data.
    // 2018
    //string lumi_calc_2018_loc = "GoodRunsLists/data18_13TeV/20180924/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root";
    string lumi_calc_2018_loc = "GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root";
    string lumi_calc_2018 = PathResolverFindCalibFile(lumi_calc_2018_loc);
    if(lumi_calc_2018=="") {
        cout << BMOFUNC << "    ERROR PathResolver unable to find "
            << "2018 lumicalc file (=" << lumi_calc_2018_loc << ")" << endl;
        lumi_calc_ok = false;
    }
    lumi_calcs.push_back(lumi_calc_2018);
    
    if(!lumi_calc_ok) exit(1);
    
    

    lumi_calcs.clear();
    if(mc_campaign() == "mc16a")
    {
        lumi_calcs = { lumi_calc_2015, lumi_calc_2016 };
    }
    else if(mc_campaign() == "mc16d")
    {
        lumi_calcs = { lumi_calc_2017 };
    }
    else if(mc_campaign() == "mc16e")
    {
        lumi_calcs = { lumi_calc_2018 };
    }

    cout << BMOFUNC << "    Loading " << lumi_calcs.size() << " lumicalc files" << endl;
    for(auto f : lumi_calcs) {
        cout << "XaodAnalysis::prw_ilumicalc_files     > " << f << endl;
    }

     return lumi_calcs;
}
//____________________________________________________________________________
vector<string> X2HLoop::prw_config_files()
{
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess())
    {
        cout << BMOFUNC << "   ERROR Failed to retrieve EventInfo object" << endl;
        exit(1);
    }
    int dsid = ei->mcChannelNumber();
    m_dsid = dsid;

    string sim_type = "FS"; // hardcoding!
    string prw_config_file_loc = "dev/PileupReweighting/mc16_13TeV/";
    string prw_config = PathResolverFindCalibDirectory(prw_config_file_loc);
    if(prw_config == "")
    {
        cout << BMOFUNC << "    ERROR Could not locate group are for PRW configs (=" << prw_config_file_loc << ")" << endl;
        exit(1);
    }
    prw_config = prw_config + "pileup_" + mc_campaign() + "_dsid" + std::to_string(dsid) + "_" + sim_type + ".root";
    cout << "\n" << BMOFUNC << " =========================================================" << endl;
    cout << BMOFUNC << "    Loading PRW configs:" << endl;
    cout << BMOFUNC << "     > " << prw_config << endl;

    bool file_ok = true;
    TFile test(prw_config.data(), "read");
    if(test.IsZombie())
    {
        cout << BMOFUNC << "   ERROR Unable to open PRW config file (=" << prw_config <<")" << endl;
        file_ok = false;
    }
    else
    {
        cout << BMOFUNC << "    PRW config opened:" << endl;
        test.Print();
        test.ls();
    }
    cout << BMOFUNC << " =========================================================\n" << endl;


    vector<string> out = {prw_config};

    if(mc_campaign() == "mc16d")
    {
        string actual_mu_17_loc = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root";
        string actual_mu_17 = PathResolverFindCalibFile(actual_mu_17_loc);
        if(actual_mu_17=="")
        {
            cout << BMOFUNC << "    ERROR Failed to open actualMu file for 2017 (=" << actual_mu_17_loc << ")" << endl;
            file_ok = false;
        }
        else
        {
            cout << BMOFUNC << "    Actual mu file for 2017: " << actual_mu_17 << endl;
            out.push_back(actual_mu_17);
        }
    }
    else if(mc_campaign() == "mc16e")
    {
        string actual_mu_18_loc = "GoodRunsLists/data18_13TeV/20181111/purw.actualMu.root";
        string actual_mu_18 = PathResolverFindCalibFile(actual_mu_18_loc);
        if(actual_mu_18=="")
        {
            cout << BMOFUNC << "    ERROR Failed to open actualMu file for 2018 (=" << actual_mu_18_loc << ")" << endl;
            file_ok = false;
        }
        else
        {
            cout << BMOFUNC << "    Actual mu file for 2018: " << actual_mu_18 << endl;
            out.push_back(actual_mu_18);
        }
    }

    if(!file_ok) exit(1);
    return out;
}
//____________________________________________________________________________
bool X2HLoop::initialize_susytools()
{
    string name = "BMOSUSYTools";
    _susyObj = new ST::SUSYObjDef_xAOD(name);
    cout << BMOFUNC << "    Initializing SUSYTools..." << endl;

    stringstream st_config;
    st_config << "xaod_dumper/SUSYTools_BMO.conf";
    RETURN_CHECK( GetName(), susyTool()->setProperty("ConfigFile", st_config.str()) );
    RETURN_CHECK( GetName(), susyTool()->setProperty("mcCampaign", mc_campaign()) );
    susyTool()->msg().setLevel(verbose() ? MSG::DEBUG : MSG::WARNING);

    // just hardcode the datasource for now
    ST::ISUSYObjDef_xAODTool::DataSource datasource = ST::ISUSYObjDef_xAODTool::FullSim;
    RETURN_CHECK( GetName(), susyTool()->setProperty("DataSource", datasource) );

    auto lumicalcs = ilumicalc_files();
    auto prw_configs = prw_config_files();
    RETURN_CHECK( GetName(), susyTool()->setProperty("PRWLumiCalcFiles", lumicalcs) );
    RETURN_CHECK( GetName(), susyTool()->setProperty("PRWConfigFiles", prw_configs) );

    if(susyTool()->initialize() != StatusCode::SUCCESS)
    {
        cout << BMOFUNC << "    ERROR Cannot initialize SUSYTools!" << endl;
        return false;
    }
    return true;
}
//____________________________________________________________________________
bool X2HLoop::initialize_output_file()
{
    stringstream name;

    name << outdir();
    name << "/";
    name << "bmo_output_" << dsid() << "_" << mc_campaign();
    if(mujetcorr())
    {
        name << "_mujetcorr";
    }
    if(suffix()!="")
    {
        name << "_" << suffix();
    }
    name << ".h5";

    _h5_file = std::make_unique<H5::H5File>(name.str().c_str(), H5F_ACC_TRUNC);

    cout << BMOFUNC << "    Output file initialized: " << _h5_file.get() << " " << name.str() << endl;

    return true;
}
//____________________________________________________________________________
bool X2HLoop::initialize_consumers_and_writers()
{
    _bmo_cons_input = std::make_unique<H5Utils::Consumers<bmo::BMOConsumerInput>>();
    _bmo_cons_event = std::make_unique<H5Utils::Consumers<bmo::BMOConsumerEvent>>();

    // for now let's just add these here, but in the future I want to make
    // this a bit more configurable

    _bmo_cons_input->add("jet_truthResponse", [](bmo::BMOConsumerInput bmo)
            {
                return ( acc_truthHadronPt(*bmo.jet) / bmo.jet->pt() );
            },
            NAN
    );

    _bmo_cons_input->add("jet_pt", [](bmo::BMOConsumerInput bmo)
            {
                return bmo.jet->pt() * 1e-3;
            },
            NAN
    );
    _bmo_cons_input->add("jet_truthHadronPt", [](bmo::BMOConsumerInput bmo)
            {
                return acc_truthHadronPt(*bmo.jet) * 1e-3;
            },
            NAN
    );
    _bmo_cons_input->add("jet_detEta", [](bmo::BMOConsumerInput bmo)
            {
                return (bmo.jet->jetP4(xAOD::JetConstitScaleMomentum)).eta();
            },
            NAN
    );
    _bmo_cons_input->add("jet_emfrac", [](bmo::BMOConsumerInput bmo)
            {
                float emfrac = 0.;
                bmo.jet->getAttribute(xAOD::JetAttribute::EMFrac, emfrac);
                return emfrac * 1e-3;
            },
            NAN
    );
    _bmo_cons_input->add("jet_energy", [](bmo::BMOConsumerInput bmo)
            {
                return bmo.jet->e() * 1e-3;
            },
            NAN
    );
    _bmo_cons_input->add("jet_m", [](bmo::BMOConsumerInput bmo)
            {
                return bmo.jet->m() * 1e-3;
            },
            NAN
    );
    _bmo_cons_input->add("jet_width", [](bmo::BMOConsumerInput bmo)
            {
                return acc_jetWidth(*bmo.jet);
            },
            NAN
    );
    _bmo_cons_input->add("n_tracks", [this](bmo::BMOConsumerInput bmo)
            {
                vector<int> nTrkVec;
                bmo.jet->getAttribute(xAOD::JetAttribute::NumTrkPt500, nTrkVec);
                return (susyTool()->GetPrimVtx()==0 || nTrkVec.size()==0) ? 0 : nTrkVec.at(susyTool()->GetPrimVtx()->index());
            },
            NAN
    );
    _bmo_cons_input->add("sum_trkPt", [this](bmo::BMOConsumerInput bmo)
            {
                return (susyTool()->GetPrimVtx() == 0 ? 0 : bmo.jet->getAttribute<std::vector<float>>(xAOD::JetAttribute::SumPtTrkPt500)[susyTool()->GetPrimVtx()->index()] * 1e-3);
            },
            NAN
    );
    _bmo_cons_input->add("jet_efrac", [this](bmo::BMOConsumerInput bmo)
            {
                float sum_trk_pt = (susyTool()->GetPrimVtx() == 0 ? 0 : bmo.jet->getAttribute<std::vector<float>>(xAOD::JetAttribute::SumPtTrkPt500)[susyTool()->GetPrimVtx()->index()]);
                return (sum_trk_pt / bmo.jet->pt());
            },
            NAN
    );
    _bmo_cons_input->add("jet_trackPtWidth", [this](bmo::BMOConsumerInput bmo)
            {
                return (susyTool()->GetPrimVtx() == 0 ? 0 : bmo.jet->getAttribute<std::vector<float>>(xAOD::JetAttribute::TrackWidthPt1000)[susyTool()->GetPrimVtx()->index()]);
            },
            NAN
    );
    _bmo_cons_input->add("jet_psE", [](bmo::BMOConsumerInput bmo)
            {
                auto e_samples = acc_sampleE(*bmo.jet);
                return (e_samples.at(0) + e_samples.at(4)) * 1e-3;
            },
            NAN
    );

    _bmo_cons_input->add("jet_has_muon", [](bmo::BMOConsumerInput bmo)
            {
                return (bmo.muon==nullptr ? 0 : 1);
            },
            NAN
    );

    _bmo_cons_input->add("dr_mujet", [](bmo::BMOConsumerInput bmo)
            {
                float dr = 0.;
                if(bmo.muon!=nullptr)
                {
                    dr = bmo.muon->p4().DeltaR(bmo.jet->p4());
                }
                return dr;
            },
            NAN
    );
    _bmo_cons_input->add("mu_ptRel", [this](bmo::BMOConsumerInput bmo)
            {
                float ptRel = 0.;
                if(bmo.muon!=nullptr)
                {
                    TLorentzVector j = bmo.jet->p4();
                    //auto boost_vec = j.BoostVector();
                    TLorentzVector mu = bmo.muon->p4();
                    j = j + mu;
                    //mu.Boost(-boost_vec); 
                    float angle = mu.Vect().Angle(j.Vect());
                    //ptRel = bmo.muon->p4().Vect().Mag() * sin(angle) * 1e-3;
                    float px = mu.Px();
                    float py = mu.Py();
                    float pz = mu.Pz();
                    float mu_p = sqrt(px*px + py*py + pz*pz);
                    ptRel = mu_p * sin(angle) * 1e-3;
                    //h->Fill(ptRel);
                }
                return ptRel;
            },
            NAN
    );

    _bmo_cons_input->add("mu_pt", [](bmo::BMOConsumerInput bmo)
            {
                float pt = 0.;
                if(bmo.muon!=nullptr)
                {
                    pt = bmo.muon->pt() * 1e-3;
                }
                return pt;
            },
            NAN
    );
    _bmo_cons_input->add("mu_eta", [](bmo::BMOConsumerInput bmo)
            {
                float eta = 0.;
                if(bmo.muon!=nullptr)
                {
                    eta = bmo.muon->eta();
                }
                return eta;
            },
            NAN
    );
    _bmo_cons_input->add("mu_d0sig", [](bmo::BMOConsumerInput bmo)
            {
                float d0sig = 0.;
                if(bmo.muon!=nullptr)
                {
                    d0sig = acc_d0sig(*bmo.muon);
                }
                return d0sig;
            },
            NAN
    );
    _bmo_cons_input->add("mu_z0sinTheta", [](bmo::BMOConsumerInput bmo)
            {
                float z0 = 0.;
                if(bmo.muon!=nullptr)
                {
                    z0 = acc_z0sinTheta(*bmo.muon);
                }
                return z0;
            },
            NAN
    );
    _bmo_cons_input->add("mu_ptvarconeRel", [](bmo::BMOConsumerInput bmo)
            {
                float iso = 0.;
                if(bmo.muon!=nullptr)
                {
                    iso = (acc_ptvarcone30(*bmo.muon) / bmo.muon->pt());
                }
                return iso;
            },
            NAN
    );
    _bmo_cons_input->add("mu_topoetconeRel", [](bmo::BMOConsumerInput bmo)
            {
                float iso = 0.;
                if(bmo.muon!=nullptr)
                {
                    iso = (acc_topoetcone20(*bmo.muon) / bmo.muon->pt());
                }
                return iso;
            },
            NAN
    );

    // event consumer
    _bmo_cons_event->add("event_number", [](bmo::BMOConsumerEvent bmo)
        {
            return bmo.event_number;
        },
        NAN
    );
    _bmo_cons_event->add("avg_mu", [](bmo::BMOConsumerEvent bmo)
        {
            return bmo.avg_mu;
        },
        NAN
    );
    _bmo_cons_event->add("event_weight", [](bmo::BMOConsumerEvent bmo)
        {
            return bmo.event_weight;
        },
        NAN
    );
    _bmo_cons_event->add("n_bjets", [](bmo::BMOConsumerEvent bmo)
        {
            return bmo.n_bjets;
        },
        NAN
    );
    _bmo_cons_event->add("mjj", [](bmo::BMOConsumerEvent bmo)
        {
            return bmo.mjj;
        },
        NAN
    );

    // writer
    _bmo_writer_input = std::make_unique<H5Utils::Writer<0,bmo::BMOConsumerInput>>(*_h5_file, "bmo_input", *_bmo_cons_input);
    _bmo_writer_event = std::make_unique<H5Utils::Writer<0,bmo::BMOConsumerEvent>>(*_h5_file, "bmo_event", *_bmo_cons_event);

    return true;
}
//____________________________________________________________________________
void X2HLoop::Init(TTree* tree)
{
    cout << BMOFUNC << endl;

    //initialize the TEvent object to read from our input chain
    event()->readFrom(tree);
    event()->getEntry(0); // to grab the EventInfo object

    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess())
    {
        cout << BMOFUNC << "    ERROR Failed to retrieve EventInfo" << endl;
        exit(1);
    }

    if(!initialize_susytools())
    {
        cout << BMOFUNC << "    ERROR Failed to initialize SUSYTools. Exiting." << endl;
        exit(1);
    }

    if(!initialize_output_file())
    {
        cout << BMOFUNC << "    ERROR Failed to initialize output file. Exiting." << endl;
        exit(1);
    }
    if(!initialize_consumers_and_writers())
    {
        cout << BMOFUNC << "    ERROR Failed to initialize H5 consumers and writers. Exiting." << endl;
        exit(1);
    }

    if(mujetcorr())
    {
        // mujetcorr initialization
    }
}
//____________________________________________________________________________
void X2HLoop::Begin(TTree* /*tree*/)
{
    cout << BMOFUNC << endl;
    timer()->Start();
}
//____________________________________________________________________________
void X2HLoop::Terminate()
{
    cout << "\n---------------------------------------------------------------\n" << endl;
    cout << BMOFUNC << "    Event loop completed" << endl;
    cout << BMOFUNC << endl;
    timer()->Stop();

    cout << timer_summary() << endl;

//    if(h)
//    {
//        h->SaveAs("bmo_test.root");
//    }

//    if(_bmo_writer_input)
//    {
//        _bmo_writer_input->flush();
//    }
}
//____________________________________________________________________________
bool X2HLoop::prepare_for_event(Long64_t entry_to_read)
{
    if(entry_to_read == 0)
    {
        cout << "\n---------------------------------------------------------------\n" << endl;
        cout << BMOFUNC << "    Preparing to start event loop" << endl;
    }
    clear_objects();
    event()->getEntry(entry_to_read);
    susyTool()->ApplyPRWTool();
    return true;
}
//____________________________________________________________________________
bool X2HLoop::load_objects()
{
    RETURN_CHECK(GetName(), susyTool()->GetElectrons(_electrons, _electrons_aux, true));
    RETURN_CHECK(GetName(), susyTool()->GetMuons(_muons, _muons_aux, true));
    RETURN_CHECK(GetName(), susyTool()->GetJets(_jets, _jets_aux));

    for(const auto & j : *xaod_jets())
    {
        susyTool()->IsBJet(*j);
    }
    RETURN_CHECK(GetName(), susyTool()->OverlapRemoval(xaod_electrons(), xaod_muons(), xaod_jets()));

    for(const auto & e : *xaod_electrons())
    {
        if(acc_baseline(*e)) _pre_electrons.push_back(e);
        if(acc_baseline(*e) && acc_passOR(*e)) _base_electrons.push_back(e);
        if(acc_baseline(*e) && acc_passOR(*e) && acc_signal(*e)) _sig_electrons.push_back(e);
    }
    std::sort(_pre_electrons.begin(), _pre_electrons.end(), PtGreater);
    std::sort(_base_electrons.begin(), _base_electrons.end(), PtGreater);
    std::sort(_sig_electrons.begin(), _sig_electrons.end(), PtGreater);

    for(const auto & m : *xaod_muons())
    {
        if(acc_baseline(*m)) _pre_muons.push_back(m);
        if(acc_baseline(*m) && acc_passOR(*m)) _base_muons.push_back(m);
        if(acc_baseline(*m) && acc_passOR(*m) && acc_signal(*m)) _sig_muons.push_back(m);
    }
    std::sort(_pre_muons.begin(), _pre_muons.end(), PtGreater);
    std::sort(_base_muons.begin(), _base_muons.end(), PtGreater);
    std::sort(_sig_muons.begin(), _sig_muons.end(), PtGreater);

    for(const auto & j : *xaod_jets())
    {
        if(acc_baseline(*j)) _pre_jets.push_back(j);
        if(acc_baseline(*j) && acc_passOR(*j)) _base_jets.push_back(j);
        if(acc_baseline(*j) && acc_passOR(*j) && acc_signal(*j)) _sig_jets.push_back(j);
    }
    std::sort(_pre_jets.begin(), _pre_jets.end(), PtGreaterJet);
    std::sort(_base_jets.begin(), _base_jets.end(), PtGreaterJet);
    std::sort(_sig_jets.begin(), _sig_jets.end(), PtGreaterJet);

    return true;
}
//____________________________________________________________________________
bool X2HLoop::clear_objects()
{
    store()->clear();

    _electrons = 0;
    _electrons_aux = 0;
    _muons = 0;
    _muons_aux = 0;
    _jets = 0;
    _jets_aux = 0;

    _pre_electrons.clear();
    _base_electrons.clear();
    _sig_electrons.clear();

    _pre_muons.clear();
    _base_muons.clear();
    _sig_muons.clear();

    _pre_jets.clear();
    _base_jets.clear();
    _sig_jets.clear();

    return true;
}
//____________________________________________________________________________
TLorentzVector X2HLoop::get_mu_eloss(const xAOD::Muon* mu)
{
    float eloss = 0.;
    mu->parameter(eloss, xAOD::Muon::EnergyLoss);
    float theta = mu->p4().Theta();
    float phi = mu->p4().Phi();
    float eloss_x = eloss * sin(theta) * cos(phi);
    float eloss_y = eloss * sin(theta) * sin(phi);
    float eloss_z = eloss * cos(theta);

    TLorentzVector eloss_TLV(eloss_x, eloss_y, eloss_z, eloss);
    return eloss_TLV;
}
//____________________________________________________________________________
void X2HLoop::perform_mu_in_jet_correction()
{

    //size_t jidx = 0;
    //cout << "==============================" << endl;
    //for(auto & jet : pre_jets())
    //{
    //    if(!acc_bjet(*jet)) continue;
    //    cout << "pre jet " << jidx << "   " << jet->pt() * 1e-3 << endl;
    //    jidx++;
    //}
    //jidx = 0;
    //for(auto & jet : base_jets())
    //{
    //    if(!acc_bjet(*jet)) continue;
    //    cout << "base jet " << jidx << "  " << jet->pt() * 1e-3 << endl;
    //    jidx++;
    //}
    for(auto & jet : pre_jets())
    {
        if(!acc_bjet(*jet)) continue;
        
        TLorentzVector jet_TLV = jet->p4();
        for(auto & mu : pre_muons())
        {
            TLorentzVector mu_TLV = mu->p4();
            if(mu_TLV.DeltaR(jet_TLV) < 0.4)
            {
                auto mu_eloss_TLV = get_mu_eloss(mu);
                // remove muon calo energy loss from jet
                jet_TLV = jet_TLV - mu_eloss_TLV;

                // add muon 4 momenta to jet
                jet_TLV = jet_TLV + mu_TLV;

                // I guess the setJetP4 method can't convert a TLorentzVector to JetFourMom_t...
                // see http://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODJet/test/xAODJet_Jet_test.cxx?v=21.2#0084
                xAOD::JetFourMom_t fourmom(jet_TLV.Pt(), jet_TLV.Eta(), jet_TLV.Phi(), jet_TLV.M());
                jet->setJetP4(fourmom); //(jet_TLV.Pt(), jet_TLV.Eta(), jet_TLV.Phi());
                //cout << BMOFUNC << "  UPDATING JET" << endl;
            }
        }
    }
    //jidx = 0;
    //cout << "- - - - - - - - - - - - - - - - - -" << endl;
    //for(auto & jet : pre_jets())
    //{
    //    if(!acc_bjet(*jet)) continue;
    //    cout << "jet " << jidx << "  " << jet->pt() * 1e-3 << endl;
    //    jidx++;
    //}
    //jidx = 0;
    //for(auto & jet : base_jets())
    //{
    //    if(!acc_bjet(*jet)) continue;
    //    cout << "base jet " << jidx << "  " << jet->pt() * 1e-3 << endl;
    //    jidx++;
    //}
}
//____________________________________________________________________________
const xAOD::TruthParticle* X2HLoop::get_jet_truth(const xAOD::Jet* jet)
{
    int truth_label = acc_jetTruthLabel(*jet);
    const xAOD::TruthParticleContainer* truth_particles = 0;
    if(!event()->retrieve(truth_particles, "TruthParticles").isSuccess())
    {
        cout << BMOFUNC << "   ERROR Failed to retrieve TruthParticles" << endl;
        exit(1);
    }

    // find the closest truth particle that matches the jet's truth label
    float min_dr = 25.;
    int nearest_idx = -1;
    const xAOD::TruthParticle* nearest_truth = nullptr;
    for(int i = 0; i < (int)truth_particles->size(); i++)
    {
        auto truth = truth_particles->at(i);
        if(!(truth->absPdgId() == truth_label)) continue;
        float dr = truth->p4().DeltaR(jet->p4());
        if(dr < min_dr)
        {
            min_dr = dr;
            nearest_idx = i;
        }
    }

    if(!(nearest_idx<0))
    {
        nearest_truth = truth_particles->at(nearest_idx);
        dec_truthHadronPt(*jet) = nearest_truth->pt();

        //float true_pt = nearest_truth->pt() * 1e-3;
        //float reco_pt = jet->pt() * 1e-3;
        //float dr = nearest_truth->p4().DeltaR(jet->p4());
        //int label = nearest_truth->absPdgId();
        //h->Fill(label);
    }

    return nearest_truth;
}
//____________________________________________________________________________
void X2HLoop::fill_consumers()
{
    const xAOD::EventInfo* ei = 0;
    if(!event()->retrieve(ei, "EventInfo").isSuccess())
    {
        cout << BMOFUNC << "   ERROR Failed to retrieve EventInfo object" << endl;
        exit(1);
    }
    bmo::BMOConsumerEvent cons_event;
    cons_event.event_weight = ei->mcEventWeight();
    cons_event.event_number = ei->eventNumber();
    
    

    int n_bjets = 0;
    for(const auto & jet : pre_jets())
    {
        if(!acc_bjet(*jet)) continue;

        n_bjets++;
        //auto truth_jet_particle = get_jet_truth(jet);
        get_jet_truth(jet);
        bmo::BMOConsumerInput cons_input;
        cons_input.jet = jet;


        int nearest_idx = -1;
        float min_dr = 25.;
        for(int imu = 0; imu < (int)pre_muons().size(); imu++)
        {
            auto mu = pre_muons().at(imu);
            float dr = mu->p4().DeltaR(jet->p4());
            if(dr < min_dr)
            {
                nearest_idx = (int)imu;
            }
        } // imu
        if(!(nearest_idx < 0))
        {
            auto closest_mu = pre_muons().at(nearest_idx);
            cons_input.muon = closest_mu;
        }
        else
        {
            cons_input.muon = nullptr;
        }
        _bmo_writer_input->fill(cons_input);
        _bmo_writer_event->fill(cons_event);
        n_jets_stored++;
    } // jet

    //if(n_bjets>0)
    //{
    //    float mbb = 0.0;
    //    if(n_bjets>=2)
    //    {
    //        mbb = (pre_jets().at(0)->p4() + pre_jets().at(1)->p4()).M();
    //    }
    //    cons_event.n_bjets = n_bjets;
    //    cons_event.mjj = mbb;
    //    _bmo_writer_event->fill(cons_event);
    //}


}
//____________________________________________________________________________
Bool_t X2HLoop::Process(Long64_t /*entry*/)
{
    static Long64_t chain_entry = -1;
    chain_entry++;
    prepare_for_event(chain_entry);

    if(verbose() || chain_entry % 1000 == 0)
    {
        cout << BMOFUNC << "    **** Processing entry " << setw(8) << chain_entry << " **** " << endl;
    }
    if(!load_objects())
    {
        cout << BMOFUNC << "    ERROR Failed to load physics objects" << endl;
        return false;
    }

    if(mujetcorr())
    {
        // do the muon in jet correction
        perform_mu_in_jet_correction();
    }

    fill_consumers();

    //_____________________
    // done with the event
    n_events_processed++;
    return true;
}

} // namespace bmo
