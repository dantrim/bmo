#ifndef BMO_X2HLOOP_H
#define BMO_X2HLOOP_H

//std/stl
#include <map>
#include <vector>
#include <memory>

//ROOT
#include "TSelector.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1F.h"

//xAOD
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODEventInfo/EventInfo.h"
namespace xAOD {
    class TEvent;
    class TStore;
}
#include "SUSYTools/SUSYObjDef_xAOD.h"

//ASG
#include "AsgTools/ToolHandle.h"

//Physics object containers
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"

//HDF5Utils
#include "HDF5Utils/Writer.h"

namespace bmo
{

struct BMOConsumerInput
{
    BMOConsumerInput()
    {
        jet = nullptr;
        muon = nullptr;
    }
    const xAOD::Jet* jet;
    const xAOD::Muon* muon;
};

struct BMOConsumerEvent
{
    BMOConsumerEvent()
    {
        event_number = 0;
        avg_mu = 0;
        event_weight = 1.0;
        n_bjets = 0;
        bj0_pt = 0.0;
        bj1_pt = 0.0;
        mjj = 0.0;
    }
    uint32_t event_number;
    float avg_mu;
    double event_weight;
    int n_bjets;
    float bj0_pt;
    float bj1_pt;
    float mjj;
};

struct pt_greater
{
    bool operator() (const xAOD::IParticle* a, const xAOD::IParticle* b)
            { return a->pt() > b->pt(); }
};
static pt_greater PtGreater;

struct pt_greaterJet
{
    bool operator() (const xAOD::Jet* a, const xAOD::Jet* b)
            { return a->pt() > b->pt(); }
};
static pt_greaterJet PtGreaterJet;

class X2HLoop : public TSelector
{
    public :
        X2HLoop();
        virtual ~X2HLoop(){};

        // configuration
        void set_verbose(bool doit) { m_verbose = doit; }
        bool verbose() { return m_verbose; }

        void set_suffix(std::string suf) { m_suffix = suf; }
        std::string suffix() { return m_suffix; }

        void set_output_dir(std::string dir) { m_outdir = dir; }
        std::string outdir() { return m_outdir; }

        void set_mujetcorr(bool doit) { m_mujetcorr = doit; }
        bool mujetcorr() { return m_mujetcorr; }

        int dsid() { return m_dsid; }

        TStopwatch* timer() { return &m_timer; }
        std::string timer_summary();

        xAOD::TEvent* event() { return m_event; }
        xAOD::TStore* store() { return m_store; }

        bool set_mc_campaign(std::string c);
        std::string mc_campaign() { return m_mc_campaign; }

        bool prepare_for_event(Long64_t entry);
        std::vector<std::string> ilumicalc_files();
        std::vector<std::string> prw_config_files();
        bool clear_objects();
        bool load_objects();
        bool initialize_susytools();
        bool initialize_output_file();
        bool initialize_consumers_and_writers();

        void perform_mu_in_jet_correction();
        TLorentzVector get_mu_eloss(const xAOD::Muon* mu);
        const xAOD::TruthParticle* get_jet_truth(const xAOD::Jet* jet);
        void fill_consumers();

        // Object getters
        ST::SUSYObjDef_xAOD* susyTool() { return _susyObj; }
        xAOD::ElectronContainer* xaod_electrons() { return _electrons; }
        xAOD::MuonContainer* xaod_muons() { return _muons; }
        xAOD::JetContainer* xaod_jets() { return _jets; }
        std::vector<xAOD::Muon*> pre_muons() { return _pre_muons; }
        std::vector<xAOD::Muon*> base_muons() { return _base_muons; }
        std::vector<xAOD::Muon*> sig_muons() { return _sig_muons; }
        std::vector<xAOD::Electron*> pre_electrons() { return _pre_electrons; }
        std::vector<xAOD::Electron*> base_electrons() { return _base_electrons; }
        std::vector<xAOD::Electron*> sig_electrons() { return _sig_electrons; }
        std::vector<xAOD::Jet*> pre_jets() { return _pre_jets; }
        std::vector<xAOD::Jet*> base_jets() { return _base_jets; }
        std::vector<xAOD::Jet*> sig_jets() { return _sig_jets; }

        // output file
        //H5::H5File* h5_file() { return _h5_file; }
        std::unique_ptr<H5::H5File>& h5_file() { return _h5_file; }

        // TSelector overrides
        virtual void Init(TTree* tree);
        virtual void Begin(TTree* tree);
        virtual Bool_t Notify() { return kTRUE; }
        virtual void Terminate();
        virtual Int_t Version() const { return 2; }
        virtual Bool_t Process(Long64_t entry);

    private :
        bool m_verbose;
        bool m_mujetcorr;
        int m_dsid;
        std::string m_outdir;
        std::string m_suffix;

        std::string m_mc_campaign;

        TStopwatch m_timer;

        xAOD::TEvent* m_event;
        xAOD::TStore* m_store;

        // SUSYTools instance
        ST::SUSYObjDef_xAOD* _susyObj;

        // object containers
        xAOD::ElectronContainer* _electrons;
        xAOD::ShallowAuxContainer* _electrons_aux;
        xAOD::MuonContainer* _muons;
        xAOD::ShallowAuxContainer* _muons_aux;
        xAOD::JetContainer* _jets;
        xAOD::ShallowAuxContainer* _jets_aux;

        std::vector<xAOD::Muon*> _pre_muons;
        std::vector<xAOD::Muon*> _base_muons;
        std::vector<xAOD::Muon*> _sig_muons;

        std::vector<xAOD::Electron*> _pre_electrons;
        std::vector<xAOD::Electron*> _base_electrons;
        std::vector<xAOD::Electron*> _sig_electrons;

        std::vector<xAOD::Jet*> _pre_jets;
        std::vector<xAOD::Jet*> _base_jets;
        std::vector<xAOD::Jet*> _sig_jets;

        // output file
        std::unique_ptr<H5::H5File> _h5_file;
        std::unique_ptr<H5Utils::Writer<0,bmo::BMOConsumerInput>> _bmo_writer_input;
        std::unique_ptr<H5Utils::Consumers<bmo::BMOConsumerInput>> _bmo_cons_input;
        std::unique_ptr<H5Utils::Writer<0,bmo::BMOConsumerEvent>> _bmo_writer_event;
        std::unique_ptr<H5Utils::Consumers<bmo::BMOConsumerEvent>> _bmo_cons_event;

        ///H5::H5File* _h5_file;
        ///H5Utils::Writer<0,bmo::BMOConsumerInput>* _bmo_writer;
        ///H5Utils::Consumers<bmo::BMOConsumerInput> _bmo_cons;

        // Testing
        TH1F* h;

        // stats 
        uint32_t n_events_processed;
        uint32_t n_jets_stored;

}; // class X2HLoop

} // namespace bmo

#endif
