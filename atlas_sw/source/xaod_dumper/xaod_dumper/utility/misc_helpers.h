#ifndef BMO_MISC_HELPERS_H
#define BMO_MISC_HELPERS_H

#include <string>

std::string computeMethodName(const std::string& function, const std::string& prettyFunction);
#define BMOFUNC computeMethodName(__FUNCTION__,__PRETTY_FUNCTION__).c_str()

#endif
