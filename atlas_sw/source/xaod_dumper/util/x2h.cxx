//xaod_dumper
#include "xaod_dumper/X2HLoop.h"
#include "xaod_dumper/utility/ChainHelper.h"

//std/stl
#include <iostream>
#include <fstream>
using namespace std;

//boost
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

//xAOD
#include "xAODRootAccess/Init.h"

int main(int argc, char** argv)
{
    string algo_name = string(argv[0]);
    string input_file = "";
    bool do_muon_in_jet = false;
    long long n_to_process = -1;
    bool verbose = false;
    string mc_campaign;
    string suffix = "";
    string output_dir = "./";

    namespace po = boost::program_options;
    string description = "Run BMO xaod_dumper to write HDF5 file";
    po::options_description desc(description);
    desc.add_options()
        ("help,h", "print out the help message")
        ("input,i",
            po::value(&input_file),
                "input file [DAOD, text filelist of DAODs, or directory of DAOD files]")
        ("nevents,n",
            po::value(&n_to_process)->default_value(-1),
                "number of events to process [default: all]")
        ("mujetcorr",
            "perform the muon-in-jet correction to all b-jets before writing output [default: false]")
        ("verbose,v",
            po::value(&verbose),
                "turn on verbose mode")
        ("mctype",
            po::value(&mc_campaign),
            "provide mc campaign (mc16a, mc16d, mc16e) of input sample")
        ("suffix",
            po::value(&suffix)->default_value(""),
                "provide a suffix to append to any outputs [default: none]")
        ("outdir",
            po::value(&output_dir)->default_value("./"),
                "provide an output directory to dump outputs into [default: ./]")
    ;

    po::variables_map vm;
    po::store(po::command_line_parser(argc,argv).options(desc).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
        cout << desc << endl;
        return 1;
    }
    if(!vm.count("mctype"))
    {
        cout << algo_name << "   ERROR At the moment you must manually provide the mc campaign (\"mctype\" command line argument)" << endl;
        return 1;
    }
    if(vm.count("mujetcorr"))
    {
        do_muon_in_jet = true;
    }
    if(!vm.count("input"))
    {
        cout << algo_name << "   ERROR You did not provide and input" << endl;
        return 1;
    }
    if(vm.count("verbose"))
    {
        verbose = true;
    }

    // check provided input
    std::ifstream user_input(input_file);
    if(!user_input.good())
    {
        cout << algo_name << "    ERROR Provided input (" << input_file << ") not found" << endl;
        return 1;
    }

    // check output dir
    boost::filesystem::path outdir_path(output_dir.c_str());
    if(!boost::filesystem::is_directory(outdir_path))
    {
        if(boost::filesystem::create_directory(outdir_path))
        {
            cout << endl;
            cout << algo_name << "    Output directory created: " << boost::filesystem::system_complete(outdir_path).string() << endl;
            cout << endl;
        }
        else
        {
            cout << endl;
            cout << algo_name << "    ERROR Unable to create requestd output directory (=" << output_dir << ")" << endl;
            cout << endl;
            return 1;
        }
    }

    xAOD::Init("BMOXaodDumper");
    TChain* chain = new TChain("CollectionTree");
    int file_err = ChainHelper::addInput(chain, input_file, true);
    if(file_err) return 1;
    chain->ls();
    Long64_t n_in_chain = chain->GetEntries();
    if(n_to_process < 0) n_to_process = n_in_chain;
    if(n_to_process > n_in_chain) n_to_process = n_in_chain;

    // analysis loop
    cout << "\n" << algo_name << "    Total number of events in the input chain     : " << n_in_chain << endl;
    cout << algo_name << "    Total number of events that will be processed : " << n_to_process << "\n" << endl;

    bmo::X2HLoop* looper = new bmo::X2HLoop();
    looper->set_verbose(verbose);
    looper->set_mujetcorr(do_muon_in_jet);
    looper->set_suffix(suffix);
    looper->set_output_dir(boost::filesystem::system_complete(outdir_path).string());
    if(!looper->set_mc_campaign(mc_campaign))
    {
        return 1;
    }

    cout << "\n================================================================" << endl;
    cout << algo_name << " Configuration:" << endl;
    cout << algo_name << endl;
    cout << algo_name << "  number of events to process     : " << n_to_process << endl;
    cout << algo_name << "  perform muon-in-jet correction  : " << do_muon_in_jet << endl;
    cout << algo_name << "  mc campaign                     : " << mc_campaign << endl;
    cout << algo_name << "  output directory                : " << boost::filesystem::system_complete(outdir_path).string() << endl;
    cout << algo_name << "  suffix                          : " << suffix << endl;
    cout << algo_name << "  verbose                         : " << verbose << endl;
    cout << algo_name << endl;
    cout << "================================================================\n" << endl;

    chain->Process(looper, input_file.c_str(), n_to_process);
    delete looper;
    delete chain;

    return 0;
}
