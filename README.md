# **BMO** - **b**-jet **mo**mentum correction tool

## Converting an Input DAOD Into HDF5 For Training
<details> <summary> Expand </summary>
There is a utility [x2h](https://gitlab.cern.ch/dantrim/bmo/blob/master/atlas_sw/source/xaod_dumper/util/x2h.cxx)
defined under the *atlas_sw* directory in the package [xaod_dumper](https://gitlab.cern.ch/dantrim/bmo/tree/master/atlas_sw/source/xaod_dumper).
This package is a simple event loop (using ROOT's [TSelector](https://root.cern.ch/developing-tselector) class) over DAOD files.
It builds electrons, muons, and jets using [SUSYTools](https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools),
which takes as input a configuration file. At the moment this configuration file is hardcoded to be the [one in xaod_dumper](https://gitlab.cern.ch/dantrim/bmo/blob/master/atlas_sw/source/xaod_dumper/data/SUSYTools_BMO.conf).
I don't see why I won't make this configurable in the future.

The output HDF5 file is written out using [HDFUtils](https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/AnalysisCommon/HDF5Utils) and
at the moment the variables that are stored are hardcoded.

### Compiling xaod_dumper

You need an AnalysisBase release `>=21.2.55`. Run the following steps in order to obtain and compile `xaod_dumper`:

```bash
git clone https://:@gitlab.cern.ch:8443/dantrim/bmo.git
cd bmo/atlas_sw/source/
lsetup "asetup AnalysisBase,21.2.55,here"
cd ..; mkdir build; cd build;
cmake ../source
make -j
source x86*/setup.sh
```

### Using the x2h utility

Once the installation succeeds by following the steps above, you will have the `x2h` executable in your area. The following options are:

```bash
x2h -h
Run BMO xaod_dumper to write HDF5 file:
  -h [ --help ]              print out the help message
  -i [ --input ] arg         input file [DAOD, text filelist of DAODs, or
                             directory of DAOD files]
  -n [ --nevents ] arg (=-1) number of events to process [default: all]
  --mujetcorr                perform the muon-in-jet correction to all b-jets
                             before writing output [default: false]
  -v [ --verbose ] arg       turn on verbose mode
  --mctype arg               provide mc campaign (mc16a, mc16d, mc16e) of input
                             sample
  --suffix arg               provide a suffix to append to any outputs
                             [default: none]
  --outdir arg (=./)         provide an output directory to dump outputs into
                             [default: ./]
```

It is assumed that you are running over an MC sample. There are no checks (at the moment), so it will crash if you give it a data sample.

The option `mujetcorr` turns on the muon-in-jet correction ([here](https://gitlab.cern.ch/dantrim/bmo/blob/master/atlas_sw/source/xaod_dumper/src/X2HLoop.cxx#L717)),
which adds the 4-momenta of the nearest muon that is within dR < 0.4 of a b-tagged jet to the b-tagged jet (also, subtracting off the 4-momenta of the muon calorimeter energy loss).
At the moment, no correction due to electrons is being performed. Doing the muon-in-jet correction affects the variables being computed
for the output HDF5 file as the jet containers are altered.


</details>