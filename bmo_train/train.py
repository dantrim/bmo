#!/bin/env python
from __future__ import print_function # just in case

import sys, os
import json
import argparse

import numpy as np
import h5py
import keras

from bmo_train.train_utils import bmo_models
from bmo_train.train_utils.data_helpers import floatify
from bmo_train.train_utils.data_preprocessing import DataScaler

class TrainingConfig :
    def __init__(self, model_name = '', configuration_file = '') :
        self._model_name = model_name
        self._configuration_file = configuration_file
        self._input_files = []
        self._input_features = []
        self._target_feature = ""
        self.load(configuration_file)

    def load(self, configuration_file) :

        load_ok = True

        with open(configuration_file) as f :
            data = json.load(f)

            if 'input_features' in data :
                self._input_features = data['input_features']
                print('TrainingConfig  Model {} has {} input features'.format(self._model_name, len(self._input_features)))
            else :
                print('ERROR Configuration file does not have \"input_features\" level')
                load_ok = False

            if 'target_feature' in data :
                self._target_feature = data['target_feature']
                print('TrainingConfig  Model {} has target: {}'.format(self._model_name, self._target_feature))
            else :
                print('ERROR Configuration file does not have \"target_feature\" level')
                load_ok = False

            if 'input_files' in data :
                self._input_files = data['input_files']
                print('TrainingConfig  Model {} has {} input file(s) for training'.format(self._model_name, len(self._input_files)))

            else :
                print('ERROR Configuration file does not have \"input_files\" level')
                load_ok = False

        if not load_ok :
            sys.exit()

    def input_files(self) :
        return self._input_files

    def input_features(self) :
        return self._input_features

    def target_feature(self) :
        return self._target_feature

    def model_name(self) :
        return self._model_name

    def config_filename(self) :
        return self._configuration_file

class Sample :
    def __init__(self, name = '', input_data = None, target_data = None) :
        if input_data.dtype != np.float64 :
            raise Exception('ERROR Sample input data must be type "np.float64", input is "{}"'.format(input_data.dtype))
        self._name = name
        self._input_data = input_data
        self._target_data = target_data

    def name(self) :
        return self._name
    def input_data(self) :
        return self._input_data
    def target_data(self) :
        return self._target_data


def load_inputs(train_config, args) :

    samples = []

    for filename in train_config.input_files() :
        with h5py.File(filename, 'r', libver = 'latest') as input_file :
            if 'bmo_input' not in input_file :
                raise Exception('ERROR "bmo_input" dataset not found in input file {}'.format(filename))
            dataset = input_file['bmo_input']

            if args.verbose :
                print('Loading {} jets from {}'.format(dataset.size, filename))
            s = Sample(name = '', input_data = floatify( dataset[tuple( train_config.input_features() )], train_config.input_features() ),
                target_data = floatify( dataset[train_config.target_feature()], [train_config.target_feature()])  )
            samples.append(s)

    return samples

def build_combined_input(samples, train_config, args) :

    sample0 = samples[0]

    inputs = sample0.input_data()
    targets = sample0.target_data()

    for sample in samples[1:] :
        inputs = np.concatenate( (inputs, sample.input_data()), axis = 0 )
        targets = np.concatenate( (targets, sample.target_data()), axis = 0 )

    data_scaler = DataScaler()
    data_scaler.load(train_config.input_features(), inputs)

    #print('data_scaler yup     : {}'.format(inputs[:2]))
    #print('data_scaler features: {}'.format(data_scaler.feature_list()))
    #print('data_scaler means   : {}'.format(data_scaler.mean()))
    #print('targets             : {}'.format(targets[:2]))

    return inputs, targets

def main(args) :

    train_config = TrainingConfig(args.model, args.config)

    train_samples = load_inputs(train_config, args)
    build_combined_input(train_samples, train_config, args)

    

    

#________________________________________________
if __name__ == '__main__' :

    parser = argparse.ArgumentParser(
        description = 'Train BMO, Train!'
    )
    parser.add_argument('-c', '--config', default = '',
        help = 'Provide BMO training configuration JSON file'
    )
    parser.add_argument('--list-models', action = 'store_true',
        help = 'List available BMO models'
    )
    parser.add_argument('-m', '--model', default = 'BMO_0',
        help = 'Provide the model to train'
    )
    parser.add_argument('-v', '--verbose', action = 'store_true',
        help = 'Turn on verbose mode'
    )
    args = parser.parse_args()

    allowed_models = bmo_models.get_models()
    model_names = [m for m in allowed_models]

    if args.list_models :
        print(45 * '-')
        n_mod = len(model_names)
        print(' BMO models:')
        for imod, mod in enumerate(model_names) :
            print('  > [{0:02d}/{1:02d}] {2}'.format(imod+1,n_mod, mod))
        sys.exit()

    if args.config == '' :
        print('You did not provide a BMO training configuration file')
        sys.exit()

    if args.model not in model_names :
        print('ERROR Requested model (={}) is not an allowed BMO model'.format(args.model))
        sys.exit()

    if not os.path.isfile(args.config) :
        print('ERROR Provided BMO training configuration file not found (={})'.format(args.config))
        sys.exit()

    main(args)

    
    
