#!/bin/env python
from __future__ import print_function

import os, sys
from keras.models import Model
from keras.layers import Input, Dense, Dropout
from keras.optimizers import SGD
from keras import regularizers
from keras import initializers
import keras
import pickle

import numpy as np
seed = 347
np.random.seed(seed)

def get_models() :

    return { 'BMO_0' : BMOModel_0 }

def get_default_layer_opts() :
    return dict( activation = 'relu', kernel_initializer = initializers.VarianceScaling(scale = 1.0, mode = 'fan_in', distribution = 'normal', seed = seed))

class BMOModel :
    def __init__(self, name = '') :
        self._name = name
        self._model = None
        self._fit_history = None

    def name(self) :
        return self._name

    def model(self) :
        return self._model

    def fit_history(self) :
        return self._fit_history

    def build_model(self, n_inputs = 0, **kwargs) :
        pass

    def fit(self, input_feature_vec = None, target_vec = None, n_epochs = 100, batch_size = 2000) :
        pass

class BMOModel_0(BMOModel) :
    def __init__(self) :
        super().__init__('BMOModel_0')

    def build_model(self, n_inputs) :

        n_nodes = 100
        layer_opts = get_default_layer_opts()

        input_layer = Input( name = "InputLayer", shape = (n_inputs,) )
        x = Dense( n_nodes, **layer_opts ) (input_layer)
        x = Dense( n_nodes, **layer_opts ) (x)
        x = Dropout(0.2) (x)
        x = Dense( n_nodes, **layer_opts ) (x)
        prediction_layer = Dense( 1, name = "OutputLayer") (x)

        model = Model( inputs = input_layer, outputs = prediction_layer )
        model.compile( loss = 'mean_squared_error', optimizer = 'adadelta' )
        self._model = model

    def fit(self, input_feature_vec = None, target_vec = None, n_epochs = 100, batch_size = 2000) :

        randomize = np.arange(len(input_feature_vec))
        np.random.shuffle(randomize)
        shuffled_inputs = input_feature_vec[randomize]
        shuffled_targets = target_vec[randomize]

        frac_for_val = 0.2
        n_samples = len(shuffled_inputs)
        n_for_val = int(frac_for_val * n_samples)

        x_train, y_train = shuffled_inputs[n_for_val:], shuffled_targets[n_for_val:]
        x_val, y_val = shuffled_inputs[:n_for_val], shuffled_targets[:n_for_val]

        self._fit_history = self._model.fit(x_train, y_train, epochs = n_epochs, batch_size = batch_size, shuffle = True, validation_data = (x_val, y_val))
