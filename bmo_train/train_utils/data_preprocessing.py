#!/bin/env python
from __future__ import print_function # just in case

#sklearn
from sklearn.preprocessing import StandardScaler
from bmo_train.train_utils.data_helpers import floatify

class DataScaler :

    """
    DataScaler

    This class will hold the scaling information needed for network
    inputs.

    One must pass into a structured array of the input features.
    """

    def __init__(self) :
        self._feature_list = []
        self._mean = None
        self._scale = None

    def load(self, feature_list_names = [], input_features = None) :
        """
        Args:
            feature_list_names : list of strings for the input features
                                    to retreive from 'input_features'
            input_features : numpy structured array of inputs
        """

        self._feature_list = feature_list_names

#        inputs = floatify(input_features, feature_list_names)
        scaler = StandardScaler()
        scaler.fit(input_features)
        scales, means = scaler.scale_, scaler.mean_

        self._mean = means
        self._scale = scales

    def feature_list(self) :
        return self._feature_list

    def scale(self) :
        return self._scale

    def mean(self) :
        return self._mean
