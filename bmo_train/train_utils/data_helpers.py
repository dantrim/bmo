#!/bin/env python

def floatify(input_numpy_array, variable_names) :
    ftype = [(name, float) for name in variable_names]
    return input_numpy_array.astype(ftype).view(float).reshape(input_numpy_array.shape + (-1,))
